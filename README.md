# espeak-demo
## 本地语音合成
#### 介绍
1. 使用时需要espeak-data文件位置正确(可以默认本项目位置)
2. lib文件夹下的jtts.jar导入项目引用。
3. espeak_lib.dll，jtts.dll需要放到jdk32位(x86)的bin文件夹中
4. 另外:不知道是不支持还是没找到相关的语音导出文件或获取语音文件流的方法导致很难使用到别的平台上(然而只能选择jacob了)

![demo](https://gitee.com/acgcat/espeak-demo/raw/master/demo.png "demo")